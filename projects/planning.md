feature 1
install everything needed

feature 2
Make sure all my apps are in my project app INSTALL_APPS portion
create a super user

feature 3
create project model
makemigration/migrate

feature 4
register project model in my admin

feature 5
Create the view base on the Project Model
create a url path for your new view in your project/app urls.py
create a list template

feature 6
in my project urls.py register my redirectview for listview/"home"

feature 7
register a loginview
include the url pattern in my project urls.py
create templates/folders with a login.html template
in your settings.py put LOGIN_REDIRECT_URL with the value "home" in your settings.

feature 8
add a few more feature in the Project model,
add user pertainig to each member.

feature 9
did what you did in feature 7 but for logout view

feaure 10
create a signup function in my accounts.view
create a signup.html template

feature 11
create a task model in my task app

feature 12
register my task model in my admin

feature 13
create a view for the detailview.
create detail path in my urls.py
create a detail.html with my tables
update list.html to show number of task

feature 14
create a create view with my project model
loggin authentication needed
if created, redirect back to detail page or the project
register url paths
create a create html template
add links to my list.html for nav

feature 15
create a create view for task
login required
redict back to detail page of the task
url path for both app and project
create a tempalte for task
and a link to the list of task for create.

feature 16
create a task list view
login required
register listview url path
create a list tempalte

feature 17
create a updateview for task
update url path
handle the proper redirect to task view
modify my task template

feature 18
download markdownify
configure markdownify in settings
apply markdownify in tempaltes

feature 19
create the navigation page on the site.

Final Notes
test everything make sure it works
flake8/black
