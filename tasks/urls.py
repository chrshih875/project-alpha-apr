from django.urls import path
from tasks.views import TaskCreateView, TaskListView, TaskUpdate

# from projects.views import ProjectListView

urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdate.as_view(), name="complete_task"),
]
